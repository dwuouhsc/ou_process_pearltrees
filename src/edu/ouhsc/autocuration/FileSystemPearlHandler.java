package edu.ouhsc.autocuration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import edu.ouhsc.dwu.pearltrees.AliasPearl;
import edu.ouhsc.dwu.pearltrees.Note;
import edu.ouhsc.dwu.pearltrees.NoteIterator;
import edu.ouhsc.dwu.pearltrees.PagePearl;
import edu.ouhsc.dwu.pearltrees.PearlHandler;
import edu.ouhsc.dwu.pearltrees.Pearltrees;
import edu.ouhsc.dwu.pearltrees.RefPearl;
import edu.ouhsc.dwu.pearltrees.RootPearl;


/**
 * TODO: Document..
 * @author David
 *
 */
public class FileSystemPearlHandler implements PearlHandler {

	private static String invalidChars;
	private static final int MAX_FILENAME = 255;
	
	static {
		System.getProperty("os.name");
		
		if (isWindows()) {
		    invalidChars = "\\/:*?\"<>|";
		} else if (isMac()) {
		    invalidChars = "/:";
		} else { // assume Unix/Linux
		    invalidChars = "/";
		}
	}
	
	private static boolean isWindows() {
		String os = System.getProperty("os.name").toLowerCase();
		// windows
		return (os.indexOf("win") >= 0);
	}
 
	private static boolean isMac() {
		String os = System.getProperty("os.name").toLowerCase();
		// Mac
		return (os.indexOf("mac") >= 0);
	}
 
	@SuppressWarnings("unused")
	private static boolean isUnix() {
		String os = System.getProperty("os.name").toLowerCase();
		// linux or unix
		return (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0);
	}
	
	/* * * actual pearl handling * * */
	private File currentDir;
	
	protected FileSystemPearlHandler() {
		this(new String());
	}
	protected FileSystemPearlHandler(String outputDirectory) {
		currentDir = new File(outputDirectory);
		currentDir.mkdirs();
	}
	
	private String buildPath(String fileName) {
		byte chars[] = fileName.getBytes();
		int i;
		for (i = 0 ; i < chars.length ; i++) {
				if (invalidChars.indexOf(chars[i]) >= 0) {
					chars[i] = ' ';
				}
		}
		
		fileName= this.currentDir.getAbsolutePath() + File.separator + new String(chars).trim();
		if (fileName.length() > MAX_FILENAME) 
			return new String(fileName.getBytes(), 0, MAX_FILENAME);
		
		return fileName;
	}

	
	@Override
	public void onPearl(RootPearl rootPearl) {
		String folderName = rootPearl.getTitle();
		File pDir = null;
		try {
		 pDir = new File(buildPath(folderName));
		} catch (IllegalArgumentException iae) {
			iae.printStackTrace();
			System.out.println(buildPath(folderName).toString());
		}
		pDir.mkdir();
		currentDir = pDir;
	}

	@Override
	public void onPearl(PagePearl pagePearl) {
		String fileName = pagePearl.getTitle() + ".txt";
		File pFile = new File(buildPath(fileName));
		PrintStream o = null;
		try {
			o = new PrintStream(new FileOutputStream(pFile));
			
			o.println(pagePearl.getType() + " : " + pagePearl.getEntryDate());
			o.println(pagePearl.getTitle());
			o.println("URL:");
			o.println(pagePearl.getPageURLstr());
			o.println();
			
			// Print the notes..
			NoteIterator notes = pagePearl.getNotes();  //This method changed in Pearl.java
			Note n;
			if (notes.hasNext()) o.println("Notes:");
			while (notes.hasNext()) {
				n = notes. nextNote();
			//	o.println(n.getCreator() + " : " + n.getDate()); This line threw exception
				o.println(n.getText());
				o.println();
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	@Override
	public void onPearl(AliasPearl aliasPearl) {
		String fileName = aliasPearl.getTitle() + ".txt";
		File pFile = new File(buildPath(fileName));
		OutputStream o = null;
		try {
			o = new FileOutputStream(pFile);
			o.write(aliasPearl.getPearlURI().getBytes());
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
	}

	@Override
	public void onPearl(RefPearl refPearl) {
		String fileName = refPearl.getTitle() + ".txt";
		File pFile = new File(buildPath(fileName));
		OutputStream o = null;
		try {
			o = new FileOutputStream(pFile);
			o.write(refPearl.getPearlURI().getBytes());
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
	}

	
	/**
	 * TODO: Document..
	 * @author David
	 *
	 */
	protected static class RecursiveFSPearlHandler extends FileSystemPearlHandler {
		
		protected RecursiveFSPearlHandler(String outputDirectory) {
			super(outputDirectory);
		}
		
		protected RecursiveFSPearlHandler() {
			super(new String());
		}
		
		@Override
		public void onPearl(RefPearl refPearl) {
			RecursiveFSPearlHandler rfsph = 
					new RecursiveFSPearlHandler(
							super.currentDir.getAbsolutePath());
			
			Pearltrees.traversePearlTree(refPearl.getPearlTree(), rfsph);
		}	
	} // RecursiveFilesystemPearlHandler
	
}// FileSystemPearlHandler

