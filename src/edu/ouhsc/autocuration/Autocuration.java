package edu.ouhsc.autocuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import edu.ouhsc.dwu.pearltrees.PearlTree;
import edu.ouhsc.dwu.pearltrees.Pearltrees; 
import edu.ouhsc.autocuration.FileSystemPearlHandler.RecursiveFSPearlHandler;

public class Autocuration {
	
	// File that is loaded if no input file is specified..
	public static final String DEFAULTFILENAME 	= "pearltrees_export_hpv_small.rdf";
	// Default file the structured data is written to..
	public static final String DEFAULTOUTFILE 	= "output.csv";
	
	// Exit codes..
	// success: program completed normally..
	public static final int SUCCESS = 0x0000;
	// error: IO Exception raised..
	public static final int IOE  	= 0x0001;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		InputStream  inStream	= System.in;
		OutputStream outStream	= System.out;
		
		String inFileName 	= null;
		String outFileName	= null;
		
		boolean expand = false;
		
		// TODO: add legitimate command line parser..
		// Read args..
		if (args.length > 0)
			inFileName = args[0];
		
		if (args.length > 1)
			outFileName = args[1];
		
		expand = (args.length > 2);
		
		// SetupSteams..
		if (inFileName != null)
			try {
				System.out.println(System.getProperty("user.dir"));
                System.out.println(new File(System.getProperty("user.dir")).toURI().resolve(inFileName));
                inStream = new FileInputStream(new File(inFileName.trim()));
                
                
			} catch (IOException ioe) {
				ioe.printStackTrace(System.err);
                System.err.println(System.getProperty("user.dir"));
                System.err.println(new File(inFileName).getAbsolutePath());
                
				System.exit(IOE);
            }
//			catch (URISyntaxException urise) {
//                urise.printStackTrace(System.err);
//                System.exit(2);
//            }
		
		File outFile = null;
		PrintStream outPS = null;
		if (outFileName != null) {
			try {
				outFile = new File(outFileName);
				if (!outFile.mkdirs()) {
					outStream = new FileOutputStream(new File(outFileName));
					outPS = new PrintStream(outStream);
				}
			} catch (IOException ioe) {
				ioe.printStackTrace(System.err);
				System.err.println(ioe.getMessage());
				System.err.println(outFileName);
				//System.exit(IOE);
			}
		}
		
		// Build PearlTree reading the xml from the file at location filename..
		PearlTree rootPearlTree = Pearltrees.buildPearlTrees(inStream);
		
		// Display the content..
		
		System.out.println("Begin PearlTree\n#==================#\n");

		
		System.out.println("\nRoot Element:");
		System.out.println(rootPearlTree.toString());
		
		System.out.println("\nPearlTree written to specified file:");
		if (outPS == null) 
			writeToFileSystem(outFile.getPath(), rootPearlTree, true);
		else if (expand) 
			writeTextToStream(outPS, rootPearlTree, true);
		else 
			writeCSVtoStream(outPS, rootPearlTree, true);
		
		
		// Finish..
		System.out.println("\n#==================#\nDone!");
		
		System.exit(SUCCESS); // !
	}
	
	
	/**
	 * Print a text representation of the given PearlTree to the specified OutputStream.
	 * @param out OutputStream the data will be written to
	 * @param pt PearlTree that will be written
	 * @param recursive true if RefPearls should be followed recursively
	 */
	public static void writeTextToStream(OutputStream out, PearlTree pt, boolean recursive) {
		Pearltrees.traversePearlTree(pt, Pearltrees.getTextPearlHandler(out, recursive));
	}
	
	/**
	 * Write a CSV version of the specified PearlTree.  Each row will contian a Pearl, the
	 * depth of which will be represented by its column.
	 * @param out OutputSteam the data will be written to
	 * @param pt PearlTre that will be written
	 * @param recursive true if RefPearls should be followed recursively
	 */
	public static void writeCSVtoStream(OutputStream out, PearlTree pt, boolean recursive) {
		Pearltrees.traversePearlTree(pt, Pearltrees.getCSVTextPearlHandler(out, recursive));
	}
	
	/**
	 * Create a directory/file tree representation of the specified PearlTree.  RootPearls
	 * become folders and Page and AliasPearls become files. 
	 * @param path System dependent folder where the directory structure should begin
	 * @param pt PearlTree that will traversed
	 * @param recursive true if RefPearls should be followed recursively
	 */
	public static void writeToFileSystem(String path, PearlTree pt, boolean recursive) {
		Pearltrees.traversePearlTree(pt, getFileSystemPearlHandler(path, recursive));
	}
	
	
	public static FileSystemPearlHandler getFileSystemPearlHandler(String path, boolean recursive) {
		return recursive ? new RecursiveFSPearlHandler(path) : new FileSystemPearlHandler(path);
	}
	
	public static FileSystemPearlHandler getFileSystemPearlHandler(boolean recursive) {
		return getFileSystemPearlHandler(new String(), recursive);
	}
}